package de.zuehlke.rental.db;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovieRepository extends JpaRepository<MovieDao, Long> {
    List<MovieDao> findAllByCategory(MovieDao.Category category);
}
