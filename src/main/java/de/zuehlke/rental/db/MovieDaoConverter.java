package de.zuehlke.rental.db;

import de.zuehlke.rental.domain.Movie;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MovieDaoConverter {
    Movie fromDao(MovieDao dao);

    List<Movie> fromDaos(List<MovieDao> movie);

    MovieDao.Category toDaoCategory(Movie.Category category);
}
