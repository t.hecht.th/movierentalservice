package de.zuehlke.rental.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="movies")
public class MovieDao {
    public enum Category {CHILDREN, NEW_RELEASE, REGULAR}

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long index;

    @Column(name="name", length=64, nullable = false)
    private String name;

    @Column(name="category", nullable = false)
    @Enumerated(EnumType.STRING)
    private Category category;

    public long getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
