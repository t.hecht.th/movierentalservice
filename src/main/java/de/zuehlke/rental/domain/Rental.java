package de.zuehlke.rental.domain;

class Rental {
    private static final int REGULAR_PRICE = 2;
    private static final int REGULAR_RENT_THRESHOLD = 2;
    private static final double CHILDREN_PRICE = 1.5;
    private static final int CHILDREN_RENT_THRESHOLD = 3;
    private static final double RENT_MULTIPLIER = 1.5;
    private static final double NEW_RELEASE_MULTIPLIER = 3;
    private final Movie movie;

    private final long daysRent;

    public Rental(Movie movie, long daysRent) {
        this.movie = movie;
        this.daysRent = daysRent;
    }

    public double calculateAmount() {
        double thisAmount = 0;
        switch (this.movie.category()) {
            case REGULAR -> {
                // Pay a constant price of 2 for the first 2 days. Pay 1.5 for each further day
                thisAmount = REGULAR_PRICE;
                if (daysRent > REGULAR_RENT_THRESHOLD) {
                    thisAmount += (daysRent - REGULAR_RENT_THRESHOLD) * RENT_MULTIPLIER;
                }
            }
            case NEW_RELEASE -> thisAmount = daysRent * NEW_RELEASE_MULTIPLIER;
            case CHILDREN -> {
                // Pay a constant price of 1.5 for the first 3 days. Pay 1.5 for each further day
                thisAmount = CHILDREN_PRICE;
                if (daysRent > CHILDREN_RENT_THRESHOLD) {
                    thisAmount += (daysRent - CHILDREN_RENT_THRESHOLD) * RENT_MULTIPLIER;
                }
            }
        }
        return thisAmount;
    }

    public int calculateFrequentRenterPoints() {
        // add bonus for a two-day new release rental
        if (Movie.Category.NEW_RELEASE.equals(this.movie.category()) && daysRent > 1) {
            return 2;
        }
        return 1;
    }
}
