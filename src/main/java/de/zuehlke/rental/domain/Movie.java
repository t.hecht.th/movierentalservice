package de.zuehlke.rental.domain;

public record Movie(int index, String name, Category category) {
    public enum Category {CHILDREN, NEW_RELEASE, REGULAR}

}
