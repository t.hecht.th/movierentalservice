package de.zuehlke.rental.domain;

import de.zuehlke.rental.db.MovieDaoConverter;
import de.zuehlke.rental.db.MovieRepository;
import de.zuehlke.rental.kafka.ReportProducer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RentalService {
    private final MovieRepository movieRepository;

    private final MovieDaoConverter movieDaoConverter;

    private final ReportProducer reportProducer;

    public RentalService(MovieRepository movieRepository, MovieDaoConverter movieDaoConverter, ReportProducer reportProducer) {
        this.movieRepository = movieRepository;
        this.movieDaoConverter = movieDaoConverter;
        this.reportProducer = reportProducer;
    }

    public List<Movie> getAllMovies() {
        return movieDaoConverter.fromDaos(movieRepository.findAll());
    }

    public List<Movie> getAllMoviesByCategory(Movie.Category category) {
        var daoCategory = movieDaoConverter.toDaoCategory(category);
        var results = movieRepository.findAllByCategory(daoCategory);
        return movieDaoConverter.fromDaos(results);
    }

    public void payRent(String userName, long movieIndex, long daysRent) {
        var movie = movieDaoConverter.fromDao(movieRepository.findById(movieIndex).orElseThrow());
        var rental = new Rental(movie, daysRent);
        reportProducer.sendReport(userName, movieIndex, rental.calculateAmount(), daysRent, rental.calculateFrequentRenterPoints());
    }
}
