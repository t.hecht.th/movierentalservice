package de.zuehlke.rental.web;

import de.zuehlke.rental.domain.RentalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/api/movies")
public class RentalRestController {
    private static final Logger logger = LoggerFactory.getLogger(RentalRestController.class);

    private final MovieDtoConverter movieDtoConverter;

    private final RentalService rentalService;

    public RentalRestController(MovieDtoConverter movieDtoConverter, RentalService rentalService) {
        this.movieDtoConverter = movieDtoConverter;
        this.rentalService = rentalService;
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MovieDto>> getMovies() {
        var movies = rentalService.getAllMovies();
        var dtos = movieDtoConverter.toDtos(movies);
        return ResponseEntity.ok(dtos);
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @GetMapping(path = "/{category}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MovieDto>> getMovies(@PathVariable MovieDto.Category category) {
        var movies = rentalService.getAllMoviesByCategory(movieDtoConverter.fromDtoCategory(category));
        var dtos = movieDtoConverter.toDtos(movies);
        return ResponseEntity.ok(dtos);
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @PostMapping(path = "/rental", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> payRent(@RequestBody List<RentalDto> rentalDtos) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        for (var rentalDto : rentalDtos) {
            rentalService.payRent(userName, rentalDto.movieIndex(), rentalDto.daysRent());
        }
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Void> handleNoSuchElementException(NoSuchElementException e) {
        logger.error("NoSuchElementException occurred", e);
        return ResponseEntity.notFound().build();
    }
}
