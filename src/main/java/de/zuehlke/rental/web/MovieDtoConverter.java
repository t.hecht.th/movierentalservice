package de.zuehlke.rental.web;

import de.zuehlke.rental.domain.Movie;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MovieDtoConverter {
    List<MovieDto> toDtos(List<Movie> movie);

    Movie.Category fromDtoCategory(MovieDto.Category category);
}
