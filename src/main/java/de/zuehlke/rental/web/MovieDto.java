package de.zuehlke.rental.web;

public record MovieDto(int index, String name, Category category) {
    public enum Category {CHILDREN, NEW_RELEASE, REGULAR}

}
