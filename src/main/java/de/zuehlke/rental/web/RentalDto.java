package de.zuehlke.rental.web;

public record RentalDto(long movieIndex, long daysRent) {}
