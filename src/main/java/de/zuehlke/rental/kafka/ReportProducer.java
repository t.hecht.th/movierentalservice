package de.zuehlke.rental.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class ReportProducer {
    private final KafkaTemplate<String, ReportEvent> kafkaTemplate;
    private final String topicName;

    public ReportProducer(KafkaTemplate<String, ReportEvent> kafkaTemplate, @Value(value = "${kafka.topic}") String topicName) {
        this.kafkaTemplate = kafkaTemplate;
        this.topicName = topicName;
    }

    public void sendReport(String userName, long movieIndex, double calculatedAmount, long daysRent, int frequentRenterPoints) {
        var event = new ReportEvent(userName, movieIndex, calculatedAmount, daysRent, frequentRenterPoints, Instant.now());
        kafkaTemplate.send(topicName, event);
    }
}
