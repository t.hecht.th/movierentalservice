package de.zuehlke.rental.kafka;

import java.time.Instant;

public record ReportEvent(String userName, long movieIndex, double calculatedAmount, long daysRent,
                          int frequentRenterPoints, Instant createdAt) {
}
